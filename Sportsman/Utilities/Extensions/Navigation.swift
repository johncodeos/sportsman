import Foundation
import UIKit

extension UINavigationController {
    func setNavBarAppearance(tintColor: UIColor, barColor: UIColor) {
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = barColor
            self.navigationBar.standardAppearance = appearance
            self.navigationBar.scrollEdgeAppearance = appearance
        }
        self.navigationBar.isTranslucent = false
        self.navigationBar.tintColor = tintColor
        self.navigationBar.barTintColor = barColor
        self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: tintColor]
    }

    func setCustomNavigationTitleImage(imageName: String, tintColor: UIColor) {
        let titleImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 108, height: 15))
        titleImageView.image = UIImage(named: imageName)?.withRenderingMode(.alwaysTemplate)
        titleImageView.tintColor = tintColor
        titleImageView.backgroundColor = .clear
        titleImageView.contentMode = .scaleAspectFit
        self.navigationBar.topItem?.titleView = titleImageView
    }
}
