import Foundation

struct MockAPI {
    private static let SECRET_KEY = "618d3aa7fe09aa001744060a"

    private static let API_URL = "https://\(SECRET_KEY).mockapi.io"

    static let SPORTS_ENDPOINT = "\(API_URL)/api/sports"
}
