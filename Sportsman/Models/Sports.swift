import Foundation

typealias Sports = [Sport]

// MARK: - Sport

struct Sport: Codable {
    let id: String
    let name: String
    let events: [SportEvent]

    enum CodingKeys: String, CodingKey {
        case id = "i"
        case name = "d"
        case events = "e"
    }
}

// MARK: - SportEvent

struct SportEvent: Codable {
    let name: String
    let id: String
    let sportId: String
    let startTime: Double

    enum CodingKeys: String, CodingKey {
        case name = "d"
        case id = "i"
        case sportId = "si"
        case startTime = "tt"
    }
}
