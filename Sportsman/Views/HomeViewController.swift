import UIKit

class HomeViewController: UIViewController {
    @IBOutlet var tableView: UITableView!

    lazy var viewModel: HomeViewModel = .init()

    var collapsedIndexSet: IndexSet = []

    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        initViewModel()
        viewModel.getSports()
    }

    func initView() {
        self.navigationController?.setNavBarAppearance(tintColor: .white, barColor: .colorFromHex("#0A96A0"))
        self.navigationController?.setCustomNavigationTitleImage(imageName: R.image.sportsmanText.name, tintColor: .white)
        let leftNavBtn = UIBarButtonItem(image: R.image.user(), style: .plain, target: self, action: nil)
        navigationItem.leftBarButtonItem = leftNavBtn
        let rightNavBtn = UIBarButtonItem(image: R.image.settings(), style: .plain, target: self, action: nil)
        navigationItem.rightBarButtonItem = rightNavBtn

        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = UIColor.colorFromHex("#1C1F26")
        // tableView.estimatedRowHeight = 163
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(R.nib.sportCell)
    }

    func initViewModel() {
        viewModel.reloadTableView = { [weak self] in
            DispatchQueue.main.async {
                guard let self = self else { return }
                self.tableView.reloadData()
            }
        }

        viewModel.showErrorAlert = { [weak self] errorMessage in
            DispatchQueue.main.async {
                guard let self = self else { return }
                // create the alert
                let alert = UIAlertController(title: "Error", message: errorMessage, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
}

extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if collapsedIndexSet.contains(indexPath.row) {
            collapsedIndexSet.remove(indexPath.row)
        } else {
            collapsedIndexSet.insert(indexPath.row)
        }
        tableView.reloadRows(at: [indexPath], with: .fade)
    }
}

extension HomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.sportCellViewModels.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.sportCell, for: indexPath)!
        let cellVM = viewModel.getSportCellViewModels(at: indexPath)
        cell.cellViewModel = cellVM
        cell.isCollapsed = collapsedIndexSet.contains(indexPath.row)
        cell.delegate = self
        return cell
    }
}

extension HomeViewController: SportCellDelegate {
    func collectionView(sportEventCell: SportEventCell, sportCell: SportCell) {
        guard let indexPathTableViewCell = tableView.indexPath(for: sportCell) else { return }
        guard let indexPathColViewCell = sportCell.sportEventsCollectionView.indexPath(for: sportEventCell) else { return }
        // Change the favourite bool value
        viewModel.sportCellViewModels[indexPathTableViewCell.row].events[indexPathColViewCell.row].eventFavouriteToggled()
        // Move the object first when is marked only as favourite
        let favouriteValue = viewModel.sportCellViewModels[indexPathTableViewCell.row].events[indexPathColViewCell.row].eventFavourite
        if favouriteValue {
            viewModel.sportCellViewModels[indexPathTableViewCell.row].changeSportEventCellPosition(from: indexPathColViewCell.row, to: 0)
            // Move the item in the CollectionView with animation
            sportCell.sportEventsCollectionView.moveItem(at: indexPathColViewCell, to: IndexPath(row: 0, section: indexPathColViewCell.section))
        }
        // Pass the viewModel with the changes for the events to the array for the collectionView
        sportCell.sportEventCellViewModels = viewModel.sportCellViewModels[indexPathTableViewCell.row].events
        // Update the favourite icon in the cell
        sportEventCell.updateFavouriteState = favouriteValue
    }
}
