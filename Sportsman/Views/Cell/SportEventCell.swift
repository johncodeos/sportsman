import Foundation
import UIKit

protocol SportEventCellDelegate: AnyObject {
    func favouriteSportEventChanged(sportEventCell: SportEventCell)
}

class SportEventCell: UICollectionViewCell {
    @IBOutlet var bgView: UIView!
    @IBOutlet var eventCountdownLabel: PaddingLabel!
    @IBOutlet var favouriteImageView: UIImageView!
    @IBOutlet var eventTeam1Label: UILabel!
    @IBOutlet var eventTeam2Label: UILabel!

    weak var delegate: SportEventCellDelegate?

    private var timer: Timer?

    var cellViewModel: SportEventCellViewModel? {
        didSet {
            guard let cellVM = cellViewModel else { return }
            // Countdown
            if self.timer == nil {
                // We repeat every 0.1 seconds instead of 1 seconds because when new cells are created the placeholder 'HH:MM:SS' shows up for 1 second until the timer gets called again
                timer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { _ in
                    self.eventCountdownLabel.text = cellVM.eventCountdownTime
                }
            }
            RunLoop.main.add(timer!, forMode: RunLoop.Mode.common) // Allow timer to still running when scrolling

            // Favourite
            favouriteImageView.image = cellVM.eventFavourite ? R.image.favouriteActive() : R.image.favouriteInactive()

            // Teams Labels
            eventTeam1Label.text = cellVM.eventTeam1
            eventTeam2Label.text = cellVM.eventTeam2

            let favouriteTapGes = UITapGestureRecognizer(target: self, action: #selector(favouriteTapped(_:)))
            favouriteTapGes.numberOfTapsRequired = 1
            favouriteImageView.isUserInteractionEnabled = true
            favouriteImageView.addGestureRecognizer(favouriteTapGes)
        }
    }

    var updateFavouriteState: Bool = false {
        didSet {
            self.favouriteImageView.image = updateFavouriteState ? R.image.favouriteActive()! : R.image.favouriteInactive()!
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        initView()
    }

    func initView() {
        bgView.backgroundColor = .colorFromHex("#1C1F26")

        // Countdown
        eventCountdownLabel.layer.cornerRadius = 6
        eventCountdownLabel.textColor = .white.withAlphaComponent(0.6)
        eventCountdownLabel.layer.borderColor = UIColor.white.withAlphaComponent(0.6).cgColor
        eventCountdownLabel.layer.borderWidth = 1
        eventCountdownLabel.text = "HH:MM:SS" // Placeholder
        eventCountdownLabel.paddingTop = 4
        eventCountdownLabel.paddingLeft = 4
        eventCountdownLabel.paddingBottom = 4
        eventCountdownLabel.paddingRight = 4

        // Event Teams
        eventTeam1Label.font = UIFont.systemFont(ofSize: 12, weight: .semibold)
        eventTeam1Label.textColor = .white.withAlphaComponent(0.6)
        eventTeam2Label.font = UIFont.systemFont(ofSize: 12, weight: .semibold)
        eventTeam2Label.textColor = .white.withAlphaComponent(0.6)
    }


    @objc func favouriteTapped(_ sender: UITapGestureRecognizer) {
        delegate?.favouriteSportEventChanged(sportEventCell: self)
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        // Stop and remove the timer when it's time to create a cell
        self.timer?.invalidate()
        self.timer = nil
    }
}
