import UIKit

protocol SportCellDelegate: AnyObject {
    func collectionView(sportEventCell: SportEventCell, sportCell: SportCell)
}

class SportCell: UITableViewCell {
    @IBOutlet var sportHeaderView: UIView!
    @IBOutlet var sportNameLabel: UILabel!
    @IBOutlet var sportExpandArrowImageView: UIImageView!
    @IBOutlet var sportEventsCollectionView: UICollectionView!
    @IBOutlet var sportEventsColViewHeightConstraint: NSLayoutConstraint!

    weak var delegate: SportCellDelegate?

    var sportEventCellViewModels: [SportEventCellViewModel]?

    var cellViewModel: SportCellViewModel? {
        didSet {
            guard let cellVM = cellViewModel else { return }
            self.sportNameLabel.text = cellVM.name
            self.sportEventCellViewModels = cellVM.events
            self.sportEventsCollectionView.reloadData()
        }
    }

    var isCollapsed: Bool = false {
        didSet {
            // Rotate the ImageView 180˚ when the cell is collapsed
            self.sportExpandArrowImageView.transform = CGAffineTransform(rotationAngle: isCollapsed ? CGFloat.pi : 0)
            // Show/hide the CollectionView when you expand/collapse the cell
            self.sportEventsColViewHeightConstraint.constant = self.isCollapsed ? 0 : 130
        }
    }

    private let colViewFlowLayout: UICollectionViewFlowLayout = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.itemSize = CGSize(width: 108, height: 129)
        flowLayout.scrollDirection = .horizontal
        flowLayout.minimumLineSpacing = 0
        flowLayout.minimumInteritemSpacing = 0
        return flowLayout
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
        initView()
    }

    func initView() {
        backgroundColor = .clear
        selectionStyle = .none

        // Sport Header View
        sportHeaderView.backgroundColor = UIColor.colorFromHex("#232B36")
        sportHeaderView.isUserInteractionEnabled = true

        // Sport Expand Arrow ImageView
        sportExpandArrowImageView.image = R.image.expandArrow()

        // Sport Name
        sportNameLabel.textColor = .white
        sportNameLabel.font = UIFont.boldSystemFont(ofSize: 12)

        // Sport Events Collection View
        sportEventsCollectionView.delegate = self
        sportEventsCollectionView.dataSource = self
        sportEventsCollectionView.backgroundColor = UIColor.colorFromHex("#1C1F26")
        sportEventsCollectionView.collectionViewLayout = colViewFlowLayout
        sportEventsCollectionView.showsHorizontalScrollIndicator = false
        sportEventsCollectionView.register(R.nib.sportEventCell)
    }
}

extension SportCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sportEventCellViewModels?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.sportEventCell, for: indexPath)!
        cell.cellViewModel = sportEventCellViewModels![indexPath.row]
        cell.delegate = self
        return cell
    }
}

extension SportCell: UICollectionViewDelegate {}

extension SportCell: SportEventCellDelegate {
    func favouriteSportEventChanged(sportEventCell: SportEventCell) {
        delegate?.collectionView(sportEventCell: sportEventCell, sportCell: self)
    }
}
