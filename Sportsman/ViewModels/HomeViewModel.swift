import Foundation
import UIKit

class HomeViewModel: NSObject {
    private let sportsService: SportsServiceProtocol

    var reloadTableView: (() -> Void)?

    var sportCellViewModels = [SportCellViewModel]() {
        didSet {
            reloadTableView?()
        }
    }

    var showErrorAlert: ((_ errorMessage: String) -> Void)?

    init(sportsService: SportsServiceProtocol = SportsService()) {
        self.sportsService = SportsService()
        super.init()
    }

    func getSportCellViewModels(at indexPath: IndexPath) -> SportCellViewModel {
        return sportCellViewModels[indexPath.row]
    }

    func getSports() {
        sportsService.getSports { success, response, error in
            if success, let model = response {
                self.processFetchedSports(sports: model)
            } else {
                self.showErrorAlert?(error!)
            }
        }
    }

    func processFetchedSports(sports: Sports) {
        var vms = [SportCellViewModel]()
        for sport in sports {
            vms.append(createSportCellModel(sport: sport))
        }
        sportCellViewModels = vms
    }

    func createSportCellModel(sport: Sport) -> SportCellViewModel {
        // Sport Id
        let sportId = sport.id
        // Sport Name
        let sportName = sport.name
        // Events
        var sportEventVMs = [SportEventCellViewModel]()
        for sportEvent in sport.events {
            sportEventVMs.append(createSportEventCellViewModel(sportEvent: sportEvent))
        }

        return SportCellViewModel(
            sportId: sportId,
            sportName: sportName,
            sportEvents: sportEventVMs)
    }

    func createSportEventCellViewModel(sportEvent: SportEvent) -> SportEventCellViewModel {
        // Sport event start time
        let startTime = sportEvent.startTime
        // Sport teams
        let teams = sportEvent.name
        return SportEventCellViewModel(
            startTime: startTime,
            teams: teams)
    }
}
