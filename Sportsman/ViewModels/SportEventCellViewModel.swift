import Foundation
import UIKit

public class SportEventCellViewModel {
    private let startTime: Double
    private var favourite: Bool
    private let teams: String

    init(startTime: Double, teams: String) {
        self.startTime = startTime
        self.favourite = false // All sport events are not set to favourite by default
        self.teams = teams
    }

    public var eventFavourite: Bool {
        return favourite
    }

    public func eventFavouriteToggled() {
        favourite = !favourite
    }

    public var eventCountdownTime: String { // return in the format HH:MM:SS
        var timeLeftFormatted = "00:00:00"
        if Date() < Date(timeIntervalSince1970: startTime) {
            let timeLeft = Calendar.current.dateComponents([.hour, .minute, .second], from: Date(), to: Date(timeIntervalSince1970: startTime))
            timeLeftFormatted = String(format: "%02d:%02d:%02d", timeLeft.hour!, timeLeft.minute!, timeLeft.second!)
        }
        return timeLeftFormatted
    }

    public var eventTeam1: String {
        let teams = teams.components(separatedBy: " - ")
        return teams[0]
    }

    public var eventTeam2: String {
        let teams = teams.components(separatedBy: " - ")
        return teams[1]
    }
}
