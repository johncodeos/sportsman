import Foundation

public class SportCellViewModel {
    private var sportId: String
    private var sportName: String
    private var sportEvents: [SportEventCellViewModel]

    init(sportId: String, sportName: String, sportEvents: [SportEventCellViewModel]) {
        self.sportId = sportId
        self.sportName = sportName
        self.sportEvents = sportEvents
    }

    public var name: String {
        return setSportIcon(sportId: sportId) + sportName
    }

    public var events: [SportEventCellViewModel] {
        return sportEvents
    }

    public func changeSportEventCellPosition(from currentPosition: Int, to newPosition: Int) {
        let element = sportEvents.remove(at: currentPosition)
        sportEvents.insert(element, at: newPosition)
    }

    private func setSportIcon(sportId: String) -> String {
        switch sportId {
        case "FOOT": // Football
            return "⚽️  " // Add space between the emoji and the sport name
        case "BASK": // Basketball
            return "🏀  "
        case "TENN": // Tennis
            return "🎾  "
        case "TABL": // Table tennis
            return "🏓  "
        case "VOLL": // Volleyball
            return "🏐  "
        case "ESPS": // Esports
            return "🕹  "
        case "ICEH": // Ice hockey
            return "🏒  "
        case "BCHV": // Beach volley
            return "🏝  "
        case "BADM": // Badminton
            return "🏸  "
        default:
            return ""
        }
    }
}
