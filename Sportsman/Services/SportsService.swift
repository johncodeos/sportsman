import Alamofire
import Foundation

protocol SportsServiceProtocol {
    func getSports(completion: @escaping (_ success: Bool, _ response: Sports?, _ error: String?) -> Void)
}

class SportsService: SportsServiceProtocol {
    func getSports(completion: @escaping (Bool, Sports?, String?) -> Void) {
        AF.request(MockAPI.SPORTS_ENDPOINT)
            .validate(statusCode: 200 ..< 299)
            .responseDecodable(of: Sports.self, completionHandler: { response in
                switch response.result {
                case .success(let model):
                    completion(true, model, nil)
                case .failure(let error):
                    completion(false, nil, error.localizedDescription)
                }
            })
    }
}
