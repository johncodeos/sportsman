@testable import Sportsman
import XCTest

class ServicesTests: XCTestCase {
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        // Any test you write for XCTest can be annotated as throws and async.
        // Mark your test throws to produce an unexpected failure when your test encounters an uncaught error.
        // Mark your test async to allow awaiting for asynchronous code to complete. Check the results with assertions afterwards.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

    func testSportsService() {
        let expectation = self.expectation(description: "Get Sports Response Parse Expectation")
        let sportsService = SportsService()
        sportsService.getSports { success, results, error in
            XCTAssertNil(error, error!)
            if success, let model = results {
                XCTAssertTrue(model.count > 0)
                expectation.fulfill()
            } else {
                XCTFail()
            }
        }
        self.waitForExpectations(timeout: 10, handler: nil)
    }
}
